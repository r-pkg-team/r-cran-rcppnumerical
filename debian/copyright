Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: RcppNumerical
Upstream-Contact: Yixuan Qiu <yixuan.qiu@cos.name>
Source: https://cran.r-project.org/package=RcppNumerical

Files: *
Copyright: 2016-2019 Yixuan Qiu,
 Ralf Stubner (Integration on infinite intervals),
 Sreekumar Balan (Numerical integration library),
 Matt Beall (Numerical integration library),
 Mark Sauder (Numerical integration library),
 Naoaki Okazaki (The libLBFGS library),
 Thomas Hahn (The Cuba library)
License: GPL-2+

Files: inst/*
Copyright: 2007-2010, Naoaki Okazaki
 1990, Jorge Nocedal
License: UNKNOWN

Files: inst/include/*
Copyright: 2016 Yixuan Qiu <yixuan.qiu@cos.name>
License: MPL-2.0

Files: inst/include/integration/*
Copyright:      2019 Ralf Stubner <ralf.stubner@gmail.com>
           2016-2019 Yixuan Qiu <yixuan.qiu@cos.name>
License: MPL-2.0

Files: inst/include/optimization/*
Copyright: 2016-2019 Yixuan Qiu <yixuan.qiu@cos.name>
License: MPL-2.0

Files: inst/include/optimization/LBFGS/LineSearchBracketing.h
 inst/include/optimization/LBFGS/LineSearchNocedalWright.h
Copyright: 2016-2019 Yixuan Qiu <yixuan.qiu@cos.name>
                     Dirk Toewe <DirkToewe@GoogleMail.com>
License: MIT

Files: debian/*
Copyright: 2021 Steffen Moeller <moeller@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.


